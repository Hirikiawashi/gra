package gra.android.m_commerce.com.gra;

import java.util.List;

/**
 * Created by Andrew on 2015-04-22.
 */
public class ButtonUtil {
    public static String isSet(List<MyButton> myButtons) {
        StringBuilder string = new StringBuilder();
        if (isSameOrDiffQuantity(myButtons) == false)
            string.append("WRONG QUANTITY\n");
        if (isSameOrDiffColor(myButtons) == false)
            string.append("WRONG COLOR\n");
        if (isSameOrDiffFullfillment(myButtons) == false)
            string.append("WRONG FULLFILLMENT\n");
        if (isSameOrDiffShape(myButtons) == false)
            string.append("WRONG SHAPE\n");
        if(string.length() == 0)
            string.append("SET\n");
        return  string.toString().substring(0, string.length()-1);
    }

    public static int countCheckSum(List<MyButton> myButtons) {
        int checkSum = 1;
        for(MyButton myButton : myButtons) {
            checkSum *= myButton.getCardID(); }
        return checkSum;
    }

    public static boolean isCardsIdIsDifferent(MyButton m1, MyButton m2, MyButton m3) {
        return isDiff(m1.getCardID(), m2.getCardID(), m3.getCardID());
    }

    private static boolean isSameOrDiffQuantity(List<MyButton> result) {
        int a = result.get(0).getQuantity();
        int b = result.get(1).getQuantity();
        int c = result.get(2).getQuantity();
        if (isSameOrDiff(a, b, c)) {
            return true; }
        else {
            return false; }
    }

    private static boolean isSameOrDiffColor(List<MyButton> result) {
        int a = result.get(0).getColor();
        int b = result.get(1).getColor();
        int c = result.get(2).getColor();
        if (isSameOrDiff(a, b, c)) {
            return true; }
        else {
            return false; }
    }

    private static boolean isSameOrDiffFullfillment(List<MyButton> result) {
        int a = result.get(0).getFullfillment();
        int b = result.get(1).getFullfillment();
        int c = result.get(2).getFullfillment();
        if (isSameOrDiff(a, b, c)) {
            return true; }
        else {
            return false; }
    }

    private static boolean isSameOrDiffShape(List<MyButton> result) {
        int a = result.get(0).getShape();
        int b = result.get(1).getShape();
        int c = result.get(2).getShape();
        if (isSameOrDiff(a, b, c)) {
            return true; }
        else {
            return false; }
    }

    private static boolean isSameOrDiff(int a, int b, int c) {
        if (isDiff(a, b, c)  || isSame(a, b, c)) {
            return true; }
        else {
            return false; }
    }

    private static boolean isSame(int a, int b, int c) {   // ON ANY PROPERTY
        if(a == b && a == c && b == c) {
            return true; }
        else {
            return false; }
    }

    public static boolean isDiff(int a, int b, int c) {
        if(a != b && a != c && b != c) {
            return true; }
        else {
            return false; }
    }
}
