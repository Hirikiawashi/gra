package gra.android.m_commerce.com.gra;

import android.content.Context;
import android.view.View;
import android.widget.ImageButton;

import java.util.List;
import java.util.Random;

/**
 * Created by Andrew on 2015-03-22.
 */
public class MyButton extends ImageButton implements View.OnClickListener {
    private int  color;
    private int  shape;
    private int  fullfillment;
    private int  quantity;
    private int  cardID;
    // References
    private List<MyButton> clickedButtonsList;
    private List<CheckSum> availableSetstoFind;
    private List<MyButton> buttonsOnTheScreen;
    //private List <Tip> tips = new ArrayList<>();


    MyButton(Context context,
             List<MyButton> buttonsOnTheScreen,
             List<MyButton> clickedButtonsList,
             List<CheckSum> availableSetstoFind) {
        super(context);

        this.clickedButtonsList = clickedButtonsList;
        this.availableSetstoFind = availableSetstoFind;
        this.buttonsOnTheScreen = buttonsOnTheScreen;
        //this.tips = tips;

        do {
            cardID = new Random().nextInt(81) + 1;
        } while (isUsed(cardID, buttonsOnTheScreen) == true);

        quantity        = cardID % 3;
        fullfillment    =             ((cardID - 1) / 27) % 3;
        shape           =             ((cardID - 1) /  9) % 3;
        color           = ((cardID - (((cardID - 1) /  9) * 9)) - 1) / 3 ;
        setImageResource(getResources().getIdentifier("i" + cardID, "drawable", context.getPackageName()));
        setBackgroundColor(0x00000000);                                                                         // WHITE COLOR
        setSelected(false);
        setOnClickListener(this);
    }

    private void buttonHasBeenClicked() {
        if(clickedButtonsList.size() ==  3) {
            String message = isSetSelected();
            if(message.equals("SET")) {
                if(tryToMarkSetAsFound() == false) {
                    message = "ALREADY FOUND!";
                } else {
                    removeFoundTip(clickedButtonsList.get(0),clickedButtonsList.get(1),clickedButtonsList.get(2));
                    if(availableSetstoFind.size() == Integer.parseInt(Game.foundSetCounterButton.getText().toString())) {
                        MainActivity.youWin();
                    }
                }
            }
                MainActivity.toastMessage(message, Game.getContext());
                ClearSelectedButtons();
        }
    }

    private void removeFoundTip(MyButton m1, MyButton m2, MyButton m3) {
        Tip tip = new Tip(m1, m2, m3);
        for(Tip tip_it : Game.tips) {
            if(tip.getDescription().equals(tip_it.getDescription()))  {
                Game.tips.remove(tip_it);
            }
        }
    }

    private boolean tryToMarkSetAsFound() {
        int checksum = ButtonUtil.countCheckSum(clickedButtonsList);
        for(CheckSum checkSum : availableSetstoFind) {
            if(checkSum.getValue() == checksum) {
                if(checkSum.found() == false) {
                    checkSum.setFound(true);
                    Game.foundSetCounterButton.setText(String.valueOf(Integer.parseInt(Game.foundSetCounterButton.getText().toString()) + 1));
                    return true;
                } else {
                    return false;
                }
            }
        }
        return true;
    }

    private void addButtonToClickedList(MyButton m) {
        clickedButtonsList.add(m);
    }

    private void ClearSelectedButtons() {
        for(MyButton myButton : clickedButtonsList) {
            myButton.setBackgroundColor(0x00000000);
            myButton.setSelected(false);
        }
        clickedButtonsList.clear();
    }

    private String isSetSelected() {
        return ButtonUtil.isSet(clickedButtonsList);
    }

    private boolean isUsed(int number, List<MyButton> buttonsOnTheScreen) {
        for(MyButton myButton : buttonsOnTheScreen) {
            if( myButton.getCardID() == number) {
                return true; }
        }
        return false;
    };

    private void removeButtonFromClickedList(MyButton m) {
        clickedButtonsList.remove(m);
    }

    @Override
    public void onClick(View v) {
        if (this.isSelected() == false) {
            this.setSelected(true);
            this.setBackgroundColor(0xEE33CCFF);                                                                // BLUE COLOR
            addButtonToClickedList(this);
            buttonHasBeenClicked();
        } else {
            this.setSelected(false);
            this.setBackgroundColor(0x00000000);                                                                // WHITE COLOR
            removeButtonFromClickedList(this);
        }
    }

    public String getDescription() {
        StringBuilder string = new StringBuilder();
//        switch (getQuantity()) {
//            case 0:     string.append("THREE, ");   break;
//            case 1:     string.append("ONE, ");     break;
//            case 2:     string.append("TWO, ");     break;
//        }
//        switch(getColor()) {
//            case 0:     string.append("RED, ");     break;
//            case 1:     string.append("PURPLE, ");  break;
//            case 2:     string.append("GREEN, ");   break;
//        }
//        switch (getShape()) {
//            case 0:     string.append("WAVE, ");    break;
//            case 1:     string.append("RECT, ");    break;
//            case 2:     string.append("BEAN, ");    break;
//        }
//        switch (getFullfillment()) {
//            case 0:     string.append("FULL.");     break;
//            case 1:     string.append("HALF.");     break;
//            case 2:     string.append("EMPTY.");    break;
//        }
        switch (getQuantity()) {
            case 0:     string.append("TRZY, ");   break;
            case 1:     string.append("JEDEN, ");     break;
            case 2:     string.append("DWA, ");     break;
        }
        switch(getColor()) {
            case 0:     string.append("CZERWONE, ");     break;
            case 1:     string.append("FIOLETOWE, ");  break;
            case 2:     string.append("ZIELONE, ");   break;
        }
        switch (getShape()) {
            case 0:     string.append("FALA, ");    break;
            case 1:     string.append("ROMB, ");    break;
            case 2:     string.append("FASOLKA, ");    break;
        }
        switch (getFullfillment()) {
            case 0:     string.append("PEŁNE.");     break;
            case 1:     string.append("PÓŁ.");     break;
            case 2:     string.append("PUSTE.");    break;
        }
        return string.toString();
    }

    public int getColor() {
        return color;
    }

    public int getShape() {
        return shape;
    }

    public int getFullfillment() {
        return fullfillment;
    }

    public int getQuantity() {
        return quantity;
    }

    public int getCardID() {
        return cardID;
    }

}
