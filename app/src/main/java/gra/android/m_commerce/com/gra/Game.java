package gra.android.m_commerce.com.gra;

import android.content.Context;
import android.view.View;
import android.widget.Button;
import android.widget.GridLayout;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Andrew on 2015-04-22.
 */
public class Game {
    private static Context context;
    private GridLayout myGridLayout;
    private List<MyButton> clickedButtonsList = new ArrayList<>();
    private List<CheckSum> availableSetstoFind = new ArrayList<>();
    private List<MyButton> buttonsOnTheScreen = new ArrayList<>();
    public static List<Tip> tips = new ArrayList<>();
    public static Game game;

    /* -----------------------MENU BUTTONS---------------------------- */

    public static Button foundSetCounterButton;
    private Button resetButton;
    private Button avaliableSetsToFindCounterButton;
    private Button tipButton;

    Game(Context context, GridLayout mygridLayout) {
        this.context = context;
        this.myGridLayout = mygridLayout;
        game = this;
    }

    public static Context getContext() {
        return context;
    }

    public void run() {
        initAllApplicationButtonsAndRun();
    }

    private void initAllApplicationButtonsAndRun() {
        generateButtonsWithSets();
        initResetButton();
        initFoundSetsButton();
        initAvaliableSetsButton();
        initTipButton();
    }

    private void generateButtonsWithSets() {
        do {
            cleanAllViews();
            initImagesButtons();
            FindAllSets();
        } while(availableSetstoFind.size() < 3);     // MAX 11 (12 sets) !!!
    }

    private void cleanAllViews() {
        myGridLayout.removeAllViews();
        buttonsOnTheScreen.clear();
        availableSetstoFind.clear();
        clickedButtonsList.clear();
        tips.clear();
    }

    private void initImagesButtons() {
        int numOfCol = myGridLayout.getColumnCount();
        int numOfRow = myGridLayout.getRowCount();
        for(int y = 0; y < numOfRow; y++){
            for(int x = 0; x < numOfCol; x++) {
                MyButton myButton = new MyButton(   context,
                                                    buttonsOnTheScreen,
                                                    clickedButtonsList,
                                                    availableSetstoFind);
                    buttonsOnTheScreen.add(myButton);
                    myGridLayout.addView(myButton);

            }
        }
    }

    private void FindAllSets() {
        for(MyButton m1 : buttonsOnTheScreen) {
            for(MyButton m2 : buttonsOnTheScreen) {
                for(MyButton m3 : buttonsOnTheScreen){
                    if(ButtonUtil.isCardsIdIsDifferent(m1, m2, m3)) {
                        List<MyButton> threesome = new ArrayList<>();
                        threesome.add(m1);
                        threesome.add(m2);
                        threesome.add(m3);
                        if (ButtonUtil.isSet(threesome).equals("SET")) {
                            int checksum = ButtonUtil.countCheckSum(threesome);
                            if (isChecksumUsed(checksum) == false) {
                                availableSetstoFind.add(new CheckSum(checksum));
                                tips.add(new Tip(threesome));
                            }
                        }
                    }
                }
            }
        }
    }

    private void initResetButton() {
        resetButton = new Button(getContext());
        resetButton.setText("RESET");
        resetButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cleanAllViews();
                initAllApplicationButtonsAndRun();
            }
        });
        myGridLayout.addView(resetButton);
    }

    private void initFoundSetsButton() {
        foundSetCounterButton = new Button(getContext());
        foundSetCounterButton.setText(Integer.toString(0));
        myGridLayout.addView(foundSetCounterButton);
    }

    private String getLastTip() {
        StringBuilder message = new StringBuilder();
        if(tips.size() > 0) {
            message.append(tips.get(tips.size() - 1).getDescription());
        }
        return message.toString();
    }

    private void initTipButton() {
        tipButton = new Button(getContext());
        tipButton.setText("Tip");
        tipButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainActivity.toastMessage(
                                            getLastTip()
                                            ,context);
            }
        });
        myGridLayout.addView(tipButton);
    }

    private void initAvaliableSetsButton() {
        avaliableSetsToFindCounterButton = new Button(getContext());
        avaliableSetsToFindCounterButton.setText(Integer.toString(availableSetstoFind.size()));
        myGridLayout.addView(avaliableSetsToFindCounterButton);
    }

    private boolean isChecksumUsed(int checksum) {
        for(CheckSum checkSum : availableSetstoFind) {
            if(checkSum.getValue() == checksum)
                return true;
        }
        return false;
    }
}
