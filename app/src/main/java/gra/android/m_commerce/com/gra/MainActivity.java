package gra.android.m_commerce.com.gra;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.widget.GridLayout;
import android.widget.Toast;


public class MainActivity extends ActionBarActivity {
    public static Game game;
    public static GridLayout myGridLayout;
    public static Context context;
    public static Activity activity;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        activity = this;
        context = getApplicationContext();
        myGridLayout = (GridLayout)findViewById(R.id.imagesGrid);
        game = new Game(context, myGridLayout);
        game.run();
    }

    public static void toastMessage(String message, Context context) {
        new Toast(context).makeText(context,
                message ,
                Toast.LENGTH_SHORT).show();
    }

    public static void youWin(){
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which){
                    case DialogInterface.BUTTON_POSITIVE:
                        game = null;
                        game = new Game(context, myGridLayout);
                        game.run();
                        break;
                    case DialogInterface.BUTTON_NEGATIVE:
                        System.exit(0);
                        break;
                }
            }
        };
        AlertDialog.Builder builder = new AlertDialog.Builder(activity).setMessage("WINNER! Continue?");
        builder.setMessage("WINNER! Continue?").setPositiveButton("Yes", dialogClickListener)
                .setNegativeButton("No", dialogClickListener).show();
    }

}