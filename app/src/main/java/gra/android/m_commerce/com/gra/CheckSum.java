package gra.android.m_commerce.com.gra;

/**
 * Created by Andrew on 2015-04-02.
 */
public class CheckSum {
    private final int value;
    private boolean found;

    CheckSum(int value) {
        this.value = value;
        this.found = false;
    }

    public boolean found() {
        return found;
    }

    public int getValue() {
        return value;
    }

    public void setFound(boolean found) {
        this.found = found;
    }
}