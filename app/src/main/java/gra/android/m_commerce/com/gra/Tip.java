package gra.android.m_commerce.com.gra;

import java.util.List;

/**
 * Created by Andrew on 2015-04-25.
 */
public class Tip {
    private String description;

    Tip(MyButton m1, MyButton m2, MyButton m3) {
        StringBuilder description = new StringBuilder();
            description.append(m1.getDescription() + "\n");
            description.append(m2.getDescription() + "\n");
            description.append(m3.getDescription());
        this.description = description.toString();
    }

    Tip(List<MyButton> list) {
        Tip tip = new Tip(list.get(0), list.get(1), list.get(2));
        this.description = tip.getDescription();
    }

    public String getDescription() {
        return description;
    }
}
